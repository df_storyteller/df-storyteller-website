# DF Storyteller Website

This is the repository for creating [https://dfstoryteller.com/](https://dfstoryteller.com/) 
and [https://docs.dfstoryteller.com/](https://docs.dfstoryteller.com/).

* `dfstoryteller.com` hosts the main information about [the main DF Storyteller](https://gitlab.com/df_storyteller/df-storyteller)
* `www.dfstoryteller.com` is the same as `dfstoryteller.com`.
* `docs.dfstoryteller.com` hosts the api and Rust documentation.

## More info

More info can be found in [the main repo](https://gitlab.com/df_storyteller/df-storyteller).

## Want to contribute?

* [Contribution guidelines for this project.](https://gitlab.com/df_storyteller/df-storyteller/CONTRIBUTING.md)

Our [Code of Conduct](https://gitlab.com/df_storyteller/df-storyteller/docs/code_of_conduct.md).
